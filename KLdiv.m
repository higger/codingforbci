function dist = KLdiv(probX, probY, varargin)
% compute KL divergance between two distributions (in bits)

p = inputParser;
p.addParameter('validProbFlag', true, @islogical);
p.parse(varargin{:});

if p.Results.validProbFlag
    % is positive and normed
    isProb = @(x)(all(x >=0) && ...
        abs(sum(x(:)) - 1) < 1e-6);
    
    assert(isProb(probX), 'probX not normed');
    assert(isProb(probY), 'probY not normed');
end

assert(numel(probX) == numel(probY), 'probs not same size');

dist = sum(probX(:) .* log(probX(:) ./ probY(:))) / log(2);
end