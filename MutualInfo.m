function MI = MutualInfo(probXY)
% computes mutual info of X, Y (in bits)
%
% INPUT:
%   probXY = [numX x numY] matrix of joint distribution

% validate probXY
assert(all(probXY(:) >= 0), 'negative prob given');
assert(abs(sum(probXY(:)) - 1) < 1e-6, 'prob not normalized');

% compute mutual information
probX = sum(probXY, 1);
probY = sum(probXY, 2);
MI = probXY .* log(probXY ./ (probY * probX));
MI(isnan(MI)) = 0;
MI(isinf(MI)) = 0;
MI = sum(MI(:)) / log(2);

end

