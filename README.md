This module contains MATLAB code which simulates a BCI as a discrete memoryless channel (DMC).  


A good starting point is **DecSeqTest.m** which gives a high level overview of a decision sequence with tunable parameters.  

Note also that every function has a "test" script which demonstrates its use (though they don't "test" all the functionality).

Please feel free to contact me with any questions or comments about using this software.  We wrote it hoping it would help validate and reproduce our results, hopefully to further somebody else's research question :) Matt.Higger@gmail.com

![decisionFrameworkFlowchart.png](https://bitbucket.org/repo/jGBxdp/images/65636097-decisionFrameworkFlowchart.png)