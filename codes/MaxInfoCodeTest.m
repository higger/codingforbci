%% MaxInfoCodeTest.m
% this script finds the code which maximizes the mutual information
% between M and Y.  It does so first through an exhaustive search and then
% through a hill climbing algorithm.  It plots histograms of the mutual
% information of the codes which it evaluates


%% =========================== test - exhaustive ==========================
%% parameters
clearvars; clc;
% be careful setting the variables below, there are numSourceMsg ^
% numChannelInMsg codes to test in exhaustive search ...
numSourceMsg = 5;   
numX = 3;

%% build arbitrary input distributions - non square confusion matrix
randDistribution = @(size)(diff([0; sort(rand(size-1,1),'ascend'); 1]));

for channelMsgIdx = numX:-1:1
    probYgivenX(:,channelMsgIdx) = randDistribution(numX);
end

probM = randDistribution(numSourceMsg);

%% test
% build random code object vector, initialize 1-9 as empty
maxInfoCodeObj = MaxInfoCode(...
    'probYgivenX',probYgivenX,...
    'mode','exhaustive');

% build maxInfo code vector
[code,probMgivenY,mutualInfo,testedCodes] = ...
    maxInfoCodeObj.Build(probM); %#ok<*ASGLU>

%% output
fprintf('optimal code vector has mutual info %g bits \n',...
    maxInfoCodeObj.ComputeCodeStats(code,probM));
hist(mutualInfo)
xlabel('mutual info M Y');
ylabel('count');
title('mutual information of all possible codes');


%% =========================== test - hill climb ==========================
%% parameters
clearvars; clc;
% params below to demo for Matrix p300
numSourceMsg = 30;
numX = 11;
numHillClimbStarts = 12;   

%% build arbitrary input distributions
randDistribution = @(size)(diff([0; sort(rand(size-1,1),'ascend'); 1]));

for channelMsgIdx = numX:-1:1
    probYgivenX(:,channelMsgIdx) = randDistribution(numX);
end

probM = randDistribution(numSourceMsg);

%% test
maxInfoCodeObj(9) = MaxInfoCode(...
    'probYgivenX',probYgivenX,...
    'mode','hillClimb',...
    'numHillClimbStarts',numHillClimbStarts);

% build maxInfo vector
tic
[code,probMgivenY,mutualInfo,testedCodes] = maxInfoCodeObj(9).Build(probM);

fprintf('hill climb method took %g secs to complete \n',toc);
fprintf('optimal code vector has mutual info %g bits \n',...
    maxInfoCodeObj(9).ComputeCodeStats(code,probM));
hist(mutualInfo)
xlabel('mutual info M Y');
ylabel('count');
title('mutual information of local max (hill climb result) codes');

%% =========================== test - fast ==========================
maxInfoCodeObj = MaxInfoCode(...
    'probYgivenX',probYgivenX,...
    'mode','fast');

[code,probMgivenY,mutualInfo,testedCodes] = maxInfoCodeObj.Build(probM);


