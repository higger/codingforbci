classdef DecisionTreeCode < DMCcode
    % decision tree codes traverse a fixed tree
    %
    % This superclass encapsulates traversal behavior such that the
    % subclass need only build an initial codebook
    %
    % METHODS:
    %   Build = builds a code or traverses tree
    
    properties
        % [numSrcMsg x maxCodewordLength] matrix of codewords (of course
        % each codewords needn't have the same length, 0 is a placeholder)
        codebook
        
        % [numTrials x 1] vector of channel msgs which have been received
        % so far (prefix to what will be received codeword)
        xReceived
        
        % (1x1) logical, turns on warning messages
        verboseFlag
    end
    
    methods
        function self = DecisionTreeCode(varargin)
            self = self@DMCcode(varargin{:});

            issquare = @(x)(size(x,1) == size(x,2));
            
            p = inputParser;
            p.KeepUnmatched = true;
            p.addParameter('verboseFlag',true,@islogical);
            p.addParameter('probYgivenX',[],issquare);
            p.parse(varargin{:});
            
            self.verboseFlag = p.Results.verboseFlag;
        end
        
        function [code, probMgivenY, MI] = Build(self, probM, varargin)
            % If probX given:
            %   Builds the next code by traversing the decision tree.
            % Otherwise:
            %   Builds a full decision tree via subclass method
            %
            % INPUT
            %   probM = [numSrcMsg x 1]  prob of source msg
            %   probX = [numBrainSymbol x 1] dist of last brain symbols 
            %           (from classifier)
            %
            % OUTPUT
            %   code  = [numSrcMsg x 1] idx of brain symbol associated with
            %           each srcMsg
            %   probMgivenY = [numSrcMsg x numX] prob of each 
            %                 srcMsg given the estimated brain symbol (y)
            %   MI    = scalar, mutual info between estimated brain symbol,
            %           y, and the sourceMsg M
            
            % validate inputs
            numSrcMsg = length(probM);
            numX = size(self.probYgivenX, 2);
            
            isDistribution =  @(x)(abs(sum(x)-1) < 1e-6);
            p = inputParser;
            p.KeepUnmatched = true;
            p.addRequired('probM',isDistribution);
            p.addParameter('probX',[],...
                @(x)(isDistribution(x)&&(length(x)==numX)));
            p.parse(probM,varargin{:});
            
            if isempty(p.Results.probX)
                % build new codebook
                self.codebook = self.BuildTree(probM);
                
                % get 'code' corresponding to single trial
                code = self.codebook(:, 1);
                
                % reset xReceived
                self.xReceived = [];
                
                % compute mutual info and probMgivenY
                [MI, probMgivenY] = self.ComputeCodeStats(code, probM);
                return
            end
            
            assert(~isempty(self.codebook), ...
                'no codebook initialized, how was probX generated?');
            
            % traverse given codebook
            trialIdx = length(self.xReceived) + 1;
            
            % ensure that there is another code to give!
            if trialIdx > size(self.codebook, 2)
                error('no more valid codes!');
            end
            
            % select xReceived as the most likely brain symbol which has
            % srcMsgs associated with it
            [~, xNextSorted] = sort(p.Results.probX, 'descend');
            for xNextIdx = 1 : length(xNextSorted)
                % build next code or signal decision with nans
                xNext = xNextSorted(xNextIdx);
                xReceivedNext = [self.xReceived, xNext];
                viableSrcMsg = ismember(...
                    self.codebook(:, 1 : trialIdx),...
                    xReceivedNext, 'rows');
                
                if sum(viableSrcMsg) >= 1
                    break
                end
            end
            
            assert(sum(viableSrcMsg) >= 1, ...
                'no srcMsg associated with any brain signal');
            
            % store xReceived
            self.xReceived = xReceivedNext;
            
            if sum(viableSrcMsg) > 1
                % build next code
                code = viableSrcMsg .* self.codebook(:, trialIdx + 1);
            elseif sum(viableSrcMsg) == 1
                % leaf reached: signal decision with nan
                code = zeros(numSrcMsg,1);
                code(viableSrcMsg) = nan;
            end
            
            % compute mutual info and probMgivenY
            if ~any(isnan(code))
                [MI, probMgivenY] = self.ComputeCodeStats(code, probM);
            else
                MI = 0;
                probMgivenY = repmat(probM, [1, numX]);
            end
        end
    end
end

