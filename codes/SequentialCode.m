classdef SequentialCode < DecisionTreeCode
    % a sequential code assigns srcMsgs to brainSymbols such that each
    % srcMsg has about the same number of brainSymbols. srcMsgs are
    % assigned 'sequentially'.
    % 
    % For example, given:
    % srcMsgs = A, B, C, D, E, F, G, H
    % brainSymbols = 1, 2, 3
    % 
    % then the sequential code has:
    %
    %  A, B, C mapped to 1
    %  D, E, F mapped to 2 
    %  G, H    mapped to 3
    %
    % in other words, code = [1, 1, 1, 2, 2, 2, 3, 3]'
    %
    % METHODS:
    %   BuildTree = build a sequential tree (codebook)
    
    
    methods
        function self = SequentialCode(varargin)
            self = self@DecisionTreeCode(varargin{:});
        end
        
        function codebook = BuildTree(self, probM)
            % builds huffman code tree
            %
            % INPUT
            %   probM   = [numSrcMsg x 1] prob of srcMsg
            %
            % OUTPUT
            %   codebook = [numSrcMsg x maxCodewordLength] matrix of
            %              codewords (each row is a codeword).  Note that a
            %              0 is a placeholder value such that each codeword
            %              has the same "length".
            
            numSrcMsg = length(probM);
            numX = size(self.probYgivenX,2);
            
            % build sequential code by counting in another base
            codebook = dec2base(1:numSrcMsg, numX);
            codebook = codebook(:, end : -1 : 1);
            codebook = double(sortrows(codebook))- double('0') + 1;
            
            % this construction may give a redundant final symbol in a
            % codeword.  we remove these redundant elements
            for srcMsgIdx = 1 : numSrcMsg
                prefix = codebook(srcMsgIdx, 1 : end - 1);
                
                hasPrefixLogical = ...
                    ismember(codebook(: , 1 : end - 1), prefix, 'rows');
                if sum(hasPrefixLogical) == 1
                    codebook(srcMsgIdx, end) = 0;
                end
            end
        end
    end  
end

